const express = require("express");
const MongoClient = require("mongodb").MongoClient;
const objectId = require("mongodb").ObjectID;


const url = "mongodb://localhost:27017/";
const mongoClient = new MongoClient(url, {useNewUrlParser: true, useUnifiedTopology: true});

function saveUser(name, login, password, repeatPassword, email) {
    //подключение к бд
    mongoClient.connect(function (err, client) {
        if (err) return console.log(err);
        const collection = client.db("usersdb").collection("users");

        const user = {
            name: name,
            login: login,
            password: password,
            repeatPassword: repeatPassword,
            email: email
        };
        collection.insertOne(user, function (err, result) {
            if (err) return console.log(err);
            //res.send(user);
            console.log("отправило в бд")
        });
    });
}


function checkUser(login, password, onError, onSuccess, onSuccessNotUser) {
//подключение к БД и получение коллекции
    mongoClient.connect(function (err, client) {
        if (err) {onError(); return console.log(err);}
        const  db = client.db("usersdb");
        const collection = db.collection("users");

        collection.findOne({login:login, password:password},

        function func(err, user) {
            if (err) {
                console.log('err');
                onError();
                return console.log(err)
            } else {
                if (user) {
                    onSuccess(user);
                } else onSuccessNotUser(user);
            }
        });
    })
};

module.exports = {
    saveUser
};
module.exports = {
    checkUser
};

